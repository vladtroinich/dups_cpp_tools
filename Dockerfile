FROM conanio/clang11
RUN conan profile new default --detect && conan profile update settings.compiler.libcxx=libstdc++11 default &&  conan profile update settings.compiler.cppstd=17 default && cat ~/.conan/profiles/default
COPY conanfile.txt /tmp/
RUN cd /tmp/ && conan install --build=missing .
RUN sudo apt update && sudo apt install -y ninja-build
VOLUME [ "/source" ]
